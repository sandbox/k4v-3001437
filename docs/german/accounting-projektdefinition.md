# Projekt Drupal Accounting

# Projektdefinition

Ziel des Projekts ist es, eine Sammlung von Drupal-8-Modulen zu entwickeln, mit dem Freiberufler und kleine Unternehmen die lästigsten Aufgaben Ihrer Buchhaltung automatisieren können und stets den Überblick über Ihre Finanzen und Projekte behalten.

Damit möchten wir möglichst viele Schritte automatisieren: Das System soll Belege verwalten und es sollen sich Regeln definieren lassen, wie diese Belege automatisch verbucht werden. Eine Anbindung an Bankkonten soll es ermöglichen, offene Posten automatisch zu schließen.

Die Drupal-Module sollen ein flexibles Framework bilden, das auch für andere Projekte nützlich sein wird, z.b. als Controlling-Tool für Agenturen und Freelancer-Netzwerke oder für die Integration in SaaS-Lösungen und Commerce-Shops.

## Wunsch-Workflow

* Papierbelege sollen digital erfasst und mit Schrifterkennung (OCR) ausgelesen werden.
* Rechnungen sollen von einem Scanner-Script direkt im System abgelegt werden.
* Rechnungen, die per E-Mail eintreffen, soll das System direkt aus einem IMAP-Postfach einsammeln.
* Rechnungen sollen automatisch von Online-Plattformen abgerufen werden.
* Für die gesammelten Belege gibt es ein einfaches Dokumenten-Management.
* Rechnungsdaten sollen automatisiert ausgelesen und in einer definierten Datenstruktur abgelegt werden. Für bekannte Rechnungs-Layouts kann der User Reguläre Ausdrücke hinterlegen.
* Mit den Belegen lassen sich dann Buchungssätze erstellen -- manuell oder regelbasiert.
* Das Bankkonto binden wir über den Standard FinTS ein, das System soll regelmäßig die aktuellen Buchungsdaten abrufen.
* Denkbar wäre sogar eine Anbindung an Elster, um die Umsatzsteuervoranmeldung abzusenden.

## Was wir (in den ersten Meilensteinen) nicht umsetzen 

* Wir entwickeln keine große Buchhaltungssoftware, sondern bilden im Kern-Modul nur die Datenstruktur der Doppelten Buchführung und des Kontenrahmens ab. Drupal soll eher die Aufgaben eines Kontierungsbüros übernehme und dem Steuerberater zuarbeiten. Die fertigen Buchungsdatensätze exportieren wir dann in einem Format, dass sich etwa in Datev oder Lexware einlesen lässt.
* Wir möchten einen Werkzeugkasten haben, der zuerst unsere eigenen Probleme löst und für individuelle Kundenlösungen eingesetzt werden kann.
* Wir erstellen zunächst keine Rechnungen. Die eigenen Ausgangs-Rechnungen importieren wir auf dem selben Wege wie die Eingangsrechnungen.
* Zielgruppe für die Module sind Drupal-Nerds und Poweruser. Schicke Oberflächen für Jedermann könnten evtl. später im Rahmen einer Distribution implementiert werden.
* OCR-Funktionen integrieren wir in der ersten Version nicht, wir gehen davon aus, dass PDFs bereits den Rohtext enthalten. Mit der Open-Source-Software Tesseract ließe ich das später relativ leicht ergänzen, wenn in einem PDF der Rohtext fehlt
* CRM-Funktionen klammern wir zunächst aus, es gibt nur einfache Entities für "Geschäftspartner", welche die Standard-Felder aus den Rechnungsdaten enthalten.

# User stories

## Rolle User

### Rechnungen vom PC aus hochladen

Als User übertrage ich eine oder mehrere PDF-Rechnungen von meinem lokalen PC in den Eingangskorb des Systems. Dabei kann ich mehrere Dateien in einem Rutsch hochladen.

Drupal entnimmt den hochgeladenen Dokumenten automatisch die enthaltenen Daten, etwa den Rohtext.

### Quittung mit dem Smartphone erfassen

Als User fotografiere ich eine Quittung mit dem Smartphone 

### Rechnungen erfassen

Als User 

### Regel für das Auslesen von OCR-Rechnungsdaten erstellen/bearbeiten/löschen 


## Rolle System


Kernfunktionen

## Dokumenten-Management


## Doppelte Buchführung
* Kontenrahmen als Taxonomien (oder doch Entities?)

## Bank-Schnittstelle

## Elster-Schnittstelle
* UStVa absenden


## Export nach Datev


* Einlesen der Bank-Buchungen via FinTS
* Einfache Dokumenten-Verwaltung mit Media.

---Mehrere Dokumente hochladen.


* Modul für doppelte Buchführung, Buchungssätze erstellen.
* Export der Buchungsdatensätze nach DATEV, inklusive der verknüpften Dokumente.

* Automatisches Erstellen von Buchungsdatensätzen


Fragen:
-------

Welche Buchungsfunktionen sind minmal notwendig? Splitbuchungen?
Welche Auswertungen brauchen wir?


Als Buchhalter scanne ich eine Telekomrechnung und lade sie ins System hoch.

Als Buchhalter erhalte ich eine Rechnung per E-Mail.

Das System ruft meine Rechnungen von Amazon ab (O2).

Als User erstelle ich eine Ausgangsrechnung und lade sie hoch.

Als User lade ich mehrere PDFs hoch, um sie dem Eingangskorb hinzuzufügen.

Das System ruft meine E-Mails ab und importiert Rechnungen automatisch. -> Bounce.

Als externes Skript lade ich ein PDF über eine Schnittstelle ins System.

---

## Rechnungsdaten extrahieren

Als Buchhalter erstelle ich einen neuen Belegdatensatz. Dazu öffne ich ein PDF aus dem Eingangskorb und übertrage die Daten in die entsprechenden Felder. Aus dem PDF kann ich Daten per Copy & Paste übernehmen. Sobald die Rechnungsdaten erfasst sind verschwindtet das PDF aus dem Eingangskorb.

Wenn ich als Buchhalter einen neuen Belegdatensatz erstelle, sind die Felder bereits mit den automatisch erkannten Daten vorbelegt (pdf2invoice).

Als Buchhalter öffne ich einen Belegdatensatz und sehe wieder das Original-PDF, um die Daten zu korrigieren.

Als Buchhalter sehe ich eine Lister der Belege, die noch nicht verbucht sind.

Als Buchhalter verbuche ich einen Beleg. Der Gesamtbetrag ist im Buchungsdatensatz vorbelegt. Ich wähle das Quell- und Zielkoto aus.

Als Buchhalter öffne ich einen Belegdatensatz und sehe wieder das Original-PDF, um die Daten zu korrigieren.

Das System erstelle automatisch Buchungsdatensätz als Vorschläge, dazu sind Regeln hinterlegt.

Als Buchhalter bestätige oder ändere ich einen Buchungsdatensatz, der automatisch erstellt wurde.

Als Buchhalter kann ich auch mehere Buchungen zu einem Beleg erstellen, um den Betrag aufzusplitten.

Als Admin erstelle ich die Liste der Konten (evtl. als Import). Konten sind hierarchisch strukturiert.


Als Buchhalter sehe ich die Kontostände.
Als Buchhalter sehe ich das Journal.
Als Buchhalter exportiere ich die Buchungsdatensätz im CSV-Format für Datev.
Als Buchhalter exportiere ich die Buchungsdatensätz im Ledger-Format.

Als Administrator lege ich ein neues Bankkonto.
Das System ruft regelmäßig neue Kontenbewegungen ab.

Als Buchhalter verknüpfe ich eine Bankbewegung mit einem Beleg (oder mit einem Buchungsdatensatz?)
